package com.vetalitet.mynewsdaggerapp.network

import com.vetalitet.mynewsdaggerapp.network.RemoteDataSource.Companion.safeApiCall
import com.vetalitet.mynewsdaggerapp.network.states.ApiState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.assertEquals
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Response

@ExperimentalCoroutinesApi
class RemoteDataSourceTest {

    /*private lateinit var dataSource: RemoteDataSource

    @Before
    fun setUp() {
        dataSource = RemoteDataSource()
    }*/

    @Test
    fun `when lambda returns successfully then it should emit the result as success`() {
        runBlockingTest {
            val lambdaResult = true
            val result = safeApiCall() { lambdaResult }
            assertEquals(ApiState.Success(lambdaResult), result)
        }
    }

    @Test
    fun `when lambda returns successfully then it should emit the result as success 222`() {
        runBlockingTest {
            val result = safeApiCall() {
                throw HttpException(
                    Response.error<Any>(500, "".toResponseBody())
                )
            }
            assert(result is ApiState.Error)
            assertEquals(
                ApiState.Error(HttpResult.SERVER_ERROR).httpResult,
                (result as ApiState.Error).httpResult
            )
        }
    }

}
