package com.vetalitet.mynewsdaggerapp.features.recipes.presentation

import android.content.SharedPreferences
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.stub
import com.vetalitet.mynewsdaggerapp.data.data.RecipesDataSource
import com.vetalitet.mynewsdaggerapp.data.data.RecipesRepository
import com.vetalitet.mynewsdaggerapp.data.fake.RecipesServiceDao
import com.vetalitet.mynewsdaggerapp.features.recipes.RecipeListViewModel
import com.vetalitet.mynewsdaggerapp.network.states.PagingState
import com.vetalitet.mynewsdaggerapp.ui.states.UiState
import com.vetalitet.mynewsdaggerapp.utils.tests.rules.CoroutineTestRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.flowOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

@FlowPreview
@ExperimentalCoroutinesApi
class RecipeViewModelTest {

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var recipesDataSource: RecipesDataSource

    @Mock
    lateinit var recipesServiceDao: RecipesServiceDao

    @Mock
    lateinit var recipesRepository: RecipesRepository

    @Mock
    lateinit var sharedPreferences: SharedPreferences

    private lateinit var viewModel: RecipeListViewModel
    //private lateinit var recipesRepository: RecipesRepository

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        //recipesRepository = RecipesRepository(recipesServiceDao, recipesDataSource)

        viewModel = RecipeListViewModel(
            recipesRepository,
            sharedPreferences,
            coroutineTestRule.testCoroutineDispatcher
        )
    }

    @Test
    fun `recipes are loaded, initialLoad() from recipesRepository is called`() =
        coroutineTestRule.runBlockingTest {
            viewModel.initData()
            verify(recipesRepository).initialLoad()
        }

    @Test
    fun `recipes are loaded, something is called`() {
        recipesRepository = RecipesRepository(recipesServiceDao, recipesDataSource)
        viewModel = RecipeListViewModel(
            recipesRepository,
            sharedPreferences,
            coroutineTestRule.testCoroutineDispatcher
        )
        coroutineTestRule.runBlockingTest {
            recipesDataSource.stub {
                onBlocking { initialLoad() } doReturn flowOf(PagingState.Initial)
            }
            viewModel.initData()
            assert(viewModel.initialLoadRecipesLiveData.value is UiState.Loading)
            //assert(viewModel.initialLoadRecipesLiveData.value is UiState.Success)
        }
    }

}
