package com.vetalitet.mynewsdaggerapp.features.testvm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.isA
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.stub
import com.vetalitet.mynewsdaggerapp.ui.states.UiState
import com.vetalitet.mynewsdaggerapp.utils.tests.rules.CoroutineTestRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.*
import org.mockito.Mockito.times

@ExperimentalCoroutinesApi
class TestViewModelTest {

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var observer:Observer<UiState<List<String>>>

    @Captor
    private lateinit var argumentCaptor: ArgumentCaptor<UiState<List<String>>>

    @Mock
    lateinit var testRepository: TestRepository

    private lateinit var viewModel: TestViewModel

    private val stubList = listOf("Test1", "Test2")

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        testRepository.stub {
            onBlocking { getInitialData() } doReturn flowOf(stubList)
        }

        viewModel = TestViewModel(testRepository, coroutineTestRule.testCoroutineDispatcher)
    }

    @Test
    fun someTest() {
        viewModel.itemsLiveData.observeForever(observer)
        viewModel.loadInitialData()

        // Note! Execution order is not defined
        Mockito.verify(observer).onChanged(isA<UiState.Loading<List<String>>>())
        Mockito.verify(observer).onChanged(isA<UiState.Success<List<String>>>())

        // Note! Execution order is not defined
        Mockito.verify(observer, times(1)).onChanged(UiState.Loading())
        Mockito.verify(observer, times(1)).onChanged(UiState.Success(stubList))
        Mockito.verify(observer, never()).onChanged(UiState.Error())

        Mockito.verify(observer, Mockito.times(2))
            .onChanged(argumentCaptor.capture())

        val values = argumentCaptor.allValues
        Assert.assertEquals(stubList, (values[1] as UiState.Success).result)
    }

}
