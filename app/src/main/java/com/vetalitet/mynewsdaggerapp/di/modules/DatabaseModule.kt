package com.vetalitet.mynewsdaggerapp.di.modules

import android.content.Context
import androidx.room.Room
import com.vetalitet.mynewsdaggerapp.data.fake.RecipesServiceDao
import com.vetalitet.mynewsdaggerapp.data.room.AppDatabase
import com.vetalitet.mynewsdaggerapp.data.room.RecipesDao
import dagger.Module
import dagger.Provides

@Module
class DatabaseModule {

    @Provides
    fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "database.db"
        ).build()
    }

    @Provides
    fun provideRecipesDao(database: AppDatabase): RecipesDao {
        return database.recipesDao()
    }

    @Provides
    fun provideRecipesServiceDao(database: AppDatabase): RecipesServiceDao {
        return database.recipesServiceDao()
    }

}
