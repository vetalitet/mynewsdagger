package com.vetalitet.mynewsdaggerapp.di

import android.app.Application
import com.vetalitet.mynewsdaggerapp.di.modules.*
import com.vetalitet.mynewsdaggerapp.features.details.RecipeDetailsFragment
import com.vetalitet.mynewsdaggerapp.features.favourites.FavouriteRecipesFragment
import com.vetalitet.mynewsdaggerapp.features.recipes.RecipeListFragment
import com.vetalitet.mynewsdaggerapp.features.testvm.TestFragment
import dagger.BindsInstance
import dagger.Component
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@FlowPreview
@Singleton
@Component(
    modules = [
        ContextModule::class,
        NetworkModule::class,
        RecipesModule::class,
        DatabaseModule::class,
        ViewModelModule::class,
        WorkManagerModule::class
    ]
)
interface CoreComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): CoreComponent
    }

    fun factory(): SampleWorkerFactory

    @ExperimentalCoroutinesApi
    @FlowPreview
    fun inject(recipeListFragment: RecipeListFragment)

    @ExperimentalCoroutinesApi
    fun inject(detailsFragment: RecipeDetailsFragment)

    @ExperimentalCoroutinesApi
    fun inject(favouriteRecipesFragment: FavouriteRecipesFragment)

    @ExperimentalCoroutinesApi
    fun inject(testFragment: TestFragment)

}
