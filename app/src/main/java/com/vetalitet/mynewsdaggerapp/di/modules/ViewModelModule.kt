package com.vetalitet.mynewsdaggerapp.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.vetalitet.mynewsdaggerapp.di.ViewModelFactory
import com.vetalitet.mynewsdaggerapp.di.ViewModelKey
import com.vetalitet.mynewsdaggerapp.features.details.RecipeDetailsViewModel
import com.vetalitet.mynewsdaggerapp.features.favourites.FavouriteRecipesViewModel
import com.vetalitet.mynewsdaggerapp.features.recipes.RecipeListViewModel
import com.vetalitet.mynewsdaggerapp.features.testvm.TestViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@FlowPreview
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @ExperimentalCoroutinesApi
    @Binds
    @IntoMap
    @ViewModelKey(RecipeListViewModel::class)
    abstract fun providesRecipeListViewModel(viewModel: RecipeListViewModel): ViewModel

    @ExperimentalCoroutinesApi
    @Binds
    @IntoMap
    @ViewModelKey(RecipeDetailsViewModel::class)
    abstract fun providesRecipeDetailsViewModel(viewModel: RecipeDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FavouriteRecipesViewModel::class)
    abstract fun providesFavouriteRecipeListViewModel(viewModel: FavouriteRecipesViewModel): ViewModel

    @ExperimentalCoroutinesApi
    @Binds
    @IntoMap
    @ViewModelKey(TestViewModel::class)
    abstract fun providesTestViewModel(viewModel: TestViewModel): ViewModel

}
