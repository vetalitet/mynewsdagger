package com.vetalitet.mynewsdaggerapp.di.modules

import com.vetalitet.mynewsdaggerapp.di.WorkerKey
import com.vetalitet.mynewsdaggerapp.features.recipes.updater.ChildWorkerFactory
import com.vetalitet.mynewsdaggerapp.features.recipes.updater.RecipesUpdaterWorker
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@Module
interface WorkManagerModule {

    @ExperimentalCoroutinesApi
    @FlowPreview
    @Binds
    @IntoMap
    @WorkerKey(RecipesUpdaterWorker::class)
    fun bindRecipesUpdaterWorker(factory: RecipesUpdaterWorker.Factory): ChildWorkerFactory

}
