package com.vetalitet.mynewsdaggerapp.di.modules

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity.MODE_PRIVATE
import com.vetalitet.mynewsdaggerapp.data.data.FavouriteRecipesDataSource
import com.vetalitet.mynewsdaggerapp.data.data.RecipesDataSource
import com.vetalitet.mynewsdaggerapp.data.fake.FakeRecipeService
import com.vetalitet.mynewsdaggerapp.data.fake.RecipesServiceDao
import com.vetalitet.mynewsdaggerapp.data.room.RecipesDao
import com.vetalitet.mynewsdaggerapp.network.RecipeService
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import retrofit2.Retrofit
import javax.inject.Singleton

@FlowPreview
@Module
class RecipesModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences =
        context.getSharedPreferences("MY_SHARED", MODE_PRIVATE)

    @Provides
    @Singleton
    fun provideRecipesService(retrofit: Retrofit): RecipeService =
        retrofit.create(RecipeService::class.java)

    @Provides
    @Singleton
    fun provideFakeRecipesService(recipesServiceDao: RecipesServiceDao): FakeRecipeService =
        FakeRecipeService(recipesServiceDao)

    @ExperimentalCoroutinesApi
    @Provides
    fun provideRecipeDataSource(
        recipeService: RecipeService,
        fakeRecipeService: FakeRecipeService,
        recipesDao: RecipesDao
    ) = RecipesDataSource(/*recipeService*/fakeRecipeService, recipesDao)

    @Provides
    fun provideFavouriteRecipeDataSource(
        recipesDao: RecipesDao
    ) = FavouriteRecipesDataSource(recipesDao)

}
