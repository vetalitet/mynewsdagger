package com.vetalitet.mynewsdaggerapp.data.mappers

import com.vetalitet.mynewsdaggerapp.data.domain.*
import com.vetalitet.mynewsdaggerapp.data.domain.Unit
import com.vetalitet.mynewsdaggerapp.data.room.RecipeEntity
import com.vetalitet.mynewsdaggerapp.data.views.custom.SectionItemData
import com.vetalitet.mynewsdaggerapp.data.views.items.AdvertisementItem
import com.vetalitet.mynewsdaggerapp.data.views.items.DataItem
import com.vetalitet.mynewsdaggerapp.data.views.items.RecipeItem
import com.vetalitet.mynewsdaggerapp.network.model.RecipeDto

fun List<RecipeDto.Recipe>.mapToDomainRecipesList(): List<Recipe> {
    return this.map { mapToDomainNewsItem(it) }
}

private fun mapToDomainNewsItem(recipe: RecipeDto.Recipe): Recipe {
    return Recipe(
        id = recipe.id ?: 0,
        name = recipe.name ?: "",
        thumbnailUrl = recipe.thumbnailUrl ?: "",
        createdAt = recipe.createdAt ?: 0,
        sections = recipe.sections.toDomainSections(),
        userRatings = recipe.rating.toRating(),
        nutrition = recipe.nutrition.toNutrition(),
        aspectRatio = recipe.aspectRatio ?: "",
        instructions = recipe.instructions.toInstructions(),
        tags = recipe.tags.toTags(),
        isFavourite = false
    )
}

private fun List<RecipeDto.Tag>?.toTags(): List<Tag> =
    this?.map { it.toTag() } ?: emptyList()

private fun RecipeDto.Tag.toTag(): Tag {
    return Tag(
        id = this.id ?: 0,
        name = this.name ?: "",
        type = this.type ?: "",
        displayName = this.displayName ?: ""
    )
}

private fun List<RecipeDto.Instruction>?.toInstructions(): List<Instruction> =
    this?.map { it.toInstruction() } ?: emptyList()

private fun RecipeDto.Instruction.toInstruction(): Instruction {
    return Instruction(
        id = this.id ?: 0,
        position = this.position ?: 0,
        displayText = this.text ?: ""
    )
}

private fun RecipeDto.Nutrition?.toNutrition(): Nutrition {
    return Nutrition(
        calories = this?.calories ?: 0,
        carbohydrates = this?.carbohydrates ?: 0,
        fat = this?.fat ?: 0,
        fiber = this?.fiber ?: 0,
        protein = this?.protein ?: 0,
        sugar = this?.sugar ?: 0
    )
}

private fun RecipeDto.Rating?.toRating(): UserRatings {
    return UserRatings(
        countPositive = this?.positive ?: 0,
        countNegative = this?.negative ?: 0,
        score = this?.score ?: 0f
    )
}

private fun List<RecipeDto.Section>.toDomainSections(): List<Section> =
    this.map { it.toSection() }

private fun RecipeDto.Section.toSection(): Section {
    return Section(
        components = this.components.toDomainComponents(),
        name = this.name ?: "",
        position = this.position ?: 0
    )
}

private fun List<RecipeDto.Component>?.toDomainComponents(): List<Component> {
    return this?.map { it.toComponent() } ?: emptyList()
}

private fun RecipeDto.Component.toComponent(): Component {
    return Component(
        id = this.id ?: 0,
        extraComment = this.extraComment ?: "",
        ingredient = this.ingredient.toIngredient(),
        measurements = this.measurements.toMeasurements(),
        position = this.position ?: 0,
        rawText = this.rawText ?: ""
    )
}

private fun List<RecipeDto.Measurements>?.toMeasurements(): List<Measurement> =
    this?.map { it.toMeasurement() } ?: emptyList()

private fun RecipeDto.Measurements.toMeasurement(): Measurement {
    return Measurement(
        id = this.id ?: 0,
        quantity = this.quantity ?: "",
        unit = this.unit.toUnit()
    )
}

private fun RecipeDto.Unit?.toUnit(): Unit {
    return Unit(
        name = this?.name ?: "",
        system = this?.system ?: ""
    )
}

private fun RecipeDto.Ingredient?.toIngredient(): Ingredient {
    return Ingredient(
        id = this?.id ?: 0,
        name = this?.name ?: ""
    )
}

fun List<Recipe>.mapToResult(): List<DataItem> {
    val result = mutableListOf<DataItem>()
    this.forEachIndexed { index, item ->
        if (index == 6) {
            result.add(AdvertisementItem(item.id))
        } else {
            result.add(
                RecipeItem(
                    item.id, item.name, item.aspectRatio, item.thumbnailUrl, item.createdAt, item.sections,
                    item.userRatings, item.nutrition
                )
            )
        }
    }
    return result
}

fun List<RecipeDto.Recipe>.toRecipeEntityList(): List<RecipeEntity> {
    return this.map { it.toRecipeEntity() }
}

private fun RecipeDto.Recipe.toRecipeEntity(): RecipeEntity {
    return RecipeEntity(
        id = id ?: 0,
        name = name ?: "",
        thumbnailUrl = thumbnailUrl ?: "",
        aspectRatio = aspectRatio ?: "",
        createdAt = createdAt ?: 0,
        instructions = instructions?.map { it.toInstruction() } ?: emptyList(),
        isFavourite = false
    )
}

fun List<RecipeEntity>.toRecipeList(): List<RecipeDto.Recipe> {
    return this.map { it.toRecipe() }
}

private fun RecipeEntity.toRecipe(): RecipeDto.Recipe {
    return RecipeDto.Recipe(
        id = this.id,
        name = this.name,
        thumbnailUrl = this.thumbnailUrl,
        aspectRatio = this.aspectRatio,
        createdAt = this.createdAt,
        instructions = this.instructions.map { it.toEntityInstructions() }
    )
}

private fun Instruction.toEntityInstructions(): RecipeDto.Instruction {
    return RecipeDto.Instruction(
        id = this.id,
        text = this.displayText,
        position = this.position
    )
}

fun Component.toSectionItemData(): SectionItemData =
    SectionItemData(position, rawText)

fun Instruction.toSectionItemData(): SectionItemData =
    SectionItemData(position, displayText)
