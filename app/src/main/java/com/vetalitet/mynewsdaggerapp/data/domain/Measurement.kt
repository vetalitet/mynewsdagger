package com.vetalitet.mynewsdaggerapp.data.domain

data class Measurement(
    val id: Int,
    val quantity: String,
    val unit: Unit
)