package com.vetalitet.mynewsdaggerapp.data.domain

data class Unit(
    val name: String,
    val system: String
)