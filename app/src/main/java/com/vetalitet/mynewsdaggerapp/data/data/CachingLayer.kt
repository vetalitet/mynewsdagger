package com.vetalitet.mynewsdaggerapp.data.data

interface CachingLayer {
    fun saveItemsToDatabase(itemsList: List<Any>, refresh: Boolean)
}
