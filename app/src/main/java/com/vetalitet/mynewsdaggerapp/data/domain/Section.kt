package com.vetalitet.mynewsdaggerapp.data.domain

data class Section(
    val components: List<Component>,
    val name: String,
    val position: Int
)