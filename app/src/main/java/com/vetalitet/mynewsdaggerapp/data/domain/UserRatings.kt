package com.vetalitet.mynewsdaggerapp.data.domain

data class UserRatings(
    val countNegative: Int,
    val countPositive: Int,
    val score: Float
)