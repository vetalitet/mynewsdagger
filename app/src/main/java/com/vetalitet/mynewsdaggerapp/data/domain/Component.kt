package com.vetalitet.mynewsdaggerapp.data.domain

data class Component(
    val extraComment: String,
    val id: Int,
    val ingredient: Ingredient,
    val measurements: List<Measurement>,
    val position: Int,
    val rawText: String
)