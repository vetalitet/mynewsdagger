package com.vetalitet.mynewsdaggerapp.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.vetalitet.mynewsdaggerapp.data.fake.RecipeServiceEntity
import com.vetalitet.mynewsdaggerapp.data.fake.RecipesServiceDao
import com.vetalitet.mynewsdaggerapp.data.room.converters.InstructionConverter

@Database(entities = [RecipeEntity::class, RecipeServiceEntity::class], version = 1)
@TypeConverters(
    value = [InstructionConverter::class]
)
abstract class AppDatabase: RoomDatabase() {
    abstract fun recipesDao(): RecipesDao
    abstract fun recipesServiceDao(): RecipesServiceDao
}
