package com.vetalitet.mynewsdaggerapp.data.room

import androidx.room.*
import com.vetalitet.mynewsdaggerapp.data.data.RecipesDataSource.Companion.PAGE_SIZE
import kotlinx.coroutines.flow.Flow

@Dao
interface RecipesDao {

    @Query("SELECT * FROM recipes ORDER BY rowIndex LIMIT :size OFFSET :from")
    fun getRecipes(from: Int, size: Int): List<RecipeEntity>

    @Query("SELECT * FROM recipes WHERE isFavourite = 1 ORDER BY rowIndex LIMIT :size OFFSET :from")
    fun getFavouriteRecipes(from: Int, size: Int): List<RecipeEntity>

    @Query("SELECT * FROM recipes ORDER BY rowIndex")
    fun getAllRecipes(): Flow<List<RecipeEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(entities: List<RecipeEntity>): List<Long>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(entity: RecipeEntity): Long

    @Update
    fun update(entity: RecipeEntity)

    @Update
    fun update(entities: List<RecipeEntity>)

    @Query("UPDATE recipes SET rowIndex = rowIndex + :delta")
    fun updateRowIndexesWithDelta(delta: Int)

    @Query("DELETE FROM recipes")
    fun deleteRecipes()

    @Transaction
    fun insertOrUpdateWithRowIndex(entities: List<RecipeEntity>, currentPage: Int, refresh: Boolean) {
        val insertResult = insert(entities)
        val updateList = mutableListOf<RecipeEntity>()

        val delay = PAGE_SIZE - entities.size
        val startPage = if (refresh) 0 else currentPage
        var rowIndexShift = 0
        for (index in insertResult.indices) {
            if (insertResult[index] != -1L) {
                val updatedEntity = entities[index]
                updatedEntity.rowIndex = (startPage * PAGE_SIZE) + delay + index + 1
                updateList.add(updatedEntity)
                rowIndexShift++
            }
        }

        if (refresh && rowIndexShift > 0) {
            updateRowIndexesWithDelta(rowIndexShift)
        }
        if (updateList.isNotEmpty()) update(updateList)
    }

}
