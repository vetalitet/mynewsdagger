package com.vetalitet.mynewsdaggerapp.data.data

import com.vetalitet.mynewsdaggerapp.cache.FetchingStrategy
import com.vetalitet.mynewsdaggerapp.cache.FetchingStrategy.*
import com.vetalitet.mynewsdaggerapp.data.data.RecipesDataSource.Companion.PAGE_SIZE
import com.vetalitet.mynewsdaggerapp.network.states.ApiState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*

@FlowPreview
abstract class BaseDataSource<Entity : Any> {

    fun fetchItems(
        from: Int,
        size: Int,
        refresh: Boolean = false,
        fetchingStrategy: FetchingStrategy = CACHE_FIRST
    ): Flow<ApiState<List<Entity>>> {
        return when (fetchingStrategy) {
            NETWORK_ONLY -> fetchAndSaveIfRequired(from, size, refresh).flowOn(Dispatchers.IO)
            CACHE_FIRST -> fetchFromCacheIfNotUseNetwork(from, size, refresh).flowOn(Dispatchers.IO)
            CACHE_ONLY -> fetchFromCache(from, size).flowOn(Dispatchers.IO)
        }
    }

    private fun fetchFromCache(from: Int, size: Int): Flow<ApiState<List<Entity>>> =
        fetchItemsFromDatabase(from, size).flatMapConcat { items ->
            flowOf(ApiState.Success(items))
        }

    private fun fetchFromCacheIfNotUseNetwork(
        from: Int,
        size: Int,
        refresh: Boolean
    ): Flow<ApiState<List<Entity>>> {
        return if (refresh) {
            fetchWithRefreshItemsFlow(from, size)
        } else {
            commonFetchItemsFlow(from, size)
        }
    }

    private fun commonFetchItemsFlow(from: Int, size: Int): Flow<ApiState<List<Entity>>> {
        return fetchItemsFromDatabase(from, size).flatMapConcat { dbEntities ->
            if (shouldFetchFromNetwork(dbEntities)) {
                fetchAndSaveIfRequired(from, size)
            } else {
                if (containsFullList(dbEntities)) {
                    flowOf(ApiState.Success(dbEntities))
                } else {
                    fetchAndSaveIfRequired(
                        from + dbEntities.size,
                        PAGE_SIZE - dbEntities.size
                    ).flatMapMerge { networkApiState ->
                        if (networkApiState is ApiState.Success) {
                            val result = mutableListOf<Entity>()
                            result.addAll(dbEntities)
                            result.addAll(networkApiState.result as List<Entity>)
                            flowOf(ApiState.Success(result))
                        } else {
                            flowOf(networkApiState)
                        }
                    }
                }
            }
        }
    }

    private fun fetchWithRefreshItemsFlow(from: Int, size: Int): Flow<ApiState<List<Entity>>> {
        return fetchAndSaveIfRequired(from, size, refresh = true)
    }

    private fun fetchAndSaveIfRequired(
        from: Int,
        size: Int,
        refresh: Boolean = false
    ): Flow<ApiState<List<Entity>>> {
        return fetchItemsFromNetwork(from, size).flatMapConcat { apiState ->
            if (this is CachingLayer) {
                if (apiState is ApiState.Success) {
                    saveItemsToDatabase(apiState.result as List<Entity>, refresh)
                }
            }
            flowOf(apiState)
        }
    }

    private fun shouldFetchFromNetwork(entities: List<Entity>) = entities.isEmpty()
    private fun containsFullList(entities: List<Entity>) = entities.size == PAGE_SIZE

    protected abstract fun fetchItemsFromNetwork(from: Int, size: Int): Flow<ApiState<List<Entity>>>
    protected abstract fun fetchItemsFromDatabase(from: Int, size: Int): Flow<List<Entity>>

}
