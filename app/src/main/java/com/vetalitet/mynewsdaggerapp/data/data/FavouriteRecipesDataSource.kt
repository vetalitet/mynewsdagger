package com.vetalitet.mynewsdaggerapp.data.data

import com.vetalitet.mynewsdaggerapp.cache.FetchingStrategy.CACHE_ONLY
import com.vetalitet.mynewsdaggerapp.data.data.RecipesDataSource.Companion.PAGE_SIZE
import com.vetalitet.mynewsdaggerapp.data.mappers.toRecipeList
import com.vetalitet.mynewsdaggerapp.data.room.RecipesDao
import com.vetalitet.mynewsdaggerapp.network.model.RecipeDto
import com.vetalitet.mynewsdaggerapp.network.states.ApiState
import com.vetalitet.mynewsdaggerapp.network.states.PagingState
import com.vetalitet.mynewsdaggerapp.utils.combineExistingListWithResult
import kotlinx.coroutines.flow.*

class FavouriteRecipesDataSource(
    private val recipesDao: RecipesDao
) : BaseDataSource<RecipeDto.Recipe>() {

    private val stateFlow = MutableStateFlow<List<RecipeDto.Recipe>>(emptyList())
    private var currentPage = 0

    fun initialLoad(): Flow<PagingState<ApiState<List<RecipeDto.Recipe>>>> =
        flow {
            emit(PagingState.Initial)
            fetchItems(0, PAGE_SIZE, fetchingStrategy = CACHE_ONLY).map { digitEntities ->
                if (digitEntities is ApiState.Success) {
                    stateFlow.value = digitEntities.result as List<RecipeDto.Recipe>
                }
                digitEntities
            }.collect { digitEntities ->
                emit(PagingState.Content(digitEntities))
                currentPage++
            }
        }

    fun loadMore(position: Int): Flow<PagingState<ApiState<List<RecipeDto.Recipe>>>> = flow {
        val cachedItems = stateFlow.value
        if (position == cachedItems.size - 1) {
            emit(PagingState.Paging(ApiState.Success(stateFlow.value)))
            fetchItems(
                currentPage * PAGE_SIZE,
                PAGE_SIZE,
                fetchingStrategy = CACHE_ONLY
            ).map { apiState ->
                if (apiState is ApiState.Success) {
                    currentPage++
                    stateFlow.value = cachedItems.combineExistingListWithResult(
                        apiState.result as List<RecipeDto.Recipe>
                    )
                    PagingState.Content(ApiState.Success(stateFlow.value))
                } else {
                    PagingState.Content(apiState)
                }
            }.collect {
                emit(it)
            }
        }
    }

    override fun fetchItemsFromNetwork(
        from: Int,
        size: Int
    ): Flow<ApiState<List<RecipeDto.Recipe>>> {
        TODO("Not yet implemented")
    }

    override fun fetchItemsFromDatabase(from: Int, size: Int): Flow<List<RecipeDto.Recipe>> = flow {
        emit(recipesDao.getFavouriteRecipes(from, size).toRecipeList())
    }

}
