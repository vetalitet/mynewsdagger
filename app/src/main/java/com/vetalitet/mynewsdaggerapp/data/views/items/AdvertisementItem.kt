package com.vetalitet.mynewsdaggerapp.data.views.items

data class AdvertisementItem(
    val id: Int
): DataItem {
    override val itemId: Int = id
}
