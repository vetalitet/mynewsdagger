package com.vetalitet.mynewsdaggerapp.data.domain

data class Recipe(
    val id: Int,
    val name: String,
    val thumbnailUrl: String,
    val aspectRatio: String,
    val createdAt: Long,
    val instructions: List<Instruction>,
    val nutrition: Nutrition,
    val sections: List<Section>,
    val userRatings: UserRatings,
    val tags: List<Tag>,
    val isFavourite: Boolean
)