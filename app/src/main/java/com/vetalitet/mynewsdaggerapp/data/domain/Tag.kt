package com.vetalitet.mynewsdaggerapp.data.domain

data class Tag(
    val displayName: String,
    val id: Int,
    val name: String,
    val type: String
)