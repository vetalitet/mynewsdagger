package com.vetalitet.mynewsdaggerapp.data.data

import com.vetalitet.mynewsdaggerapp.data.fake.FakeRecipeService
import com.vetalitet.mynewsdaggerapp.data.mappers.toRecipeEntityList
import com.vetalitet.mynewsdaggerapp.data.mappers.toRecipeList
import com.vetalitet.mynewsdaggerapp.data.room.RecipeEntity
import com.vetalitet.mynewsdaggerapp.data.room.RecipesDao
import com.vetalitet.mynewsdaggerapp.network.RemoteDataSource.Companion.safeApiCall
import com.vetalitet.mynewsdaggerapp.network.model.RecipeDto
import com.vetalitet.mynewsdaggerapp.network.states.ApiState
import com.vetalitet.mynewsdaggerapp.network.states.PagingState
import com.vetalitet.mynewsdaggerapp.utils.combineExistingListWithResult
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@FlowPreview
@ExperimentalCoroutinesApi
class RecipesDataSource @Inject constructor(
    //private val recipeService: RecipeService,
    private val fakeRecipeService: FakeRecipeService,
    private val recipesDao: RecipesDao
) : BaseDataSource<RecipeDto.Recipe>(), CachingLayer {

    private val stateFlow = MutableStateFlow<List<RecipeDto.Recipe>>(emptyList())
    private var currentPage = 0

    fun initialLoad(): Flow<PagingState<ApiState<List<RecipeDto.Recipe>>>> =
        flow {
            emit(PagingState.Initial)
            fetchItems(0, PAGE_SIZE).map { digitEntities ->
                if (digitEntities is ApiState.Success) {
                    stateFlow.value = digitEntities.result as List<RecipeDto.Recipe>
                }
                digitEntities
            }.collect { digitEntities ->
                emit(PagingState.Content(digitEntities))
                currentPage++
            }
        }

    fun loadMore(position: Int): Flow<PagingState<ApiState<List<RecipeDto.Recipe>>>> = flow {
        val cachedItems = stateFlow.value
        if (position == cachedItems.size - 1) {
            emit(PagingState.Paging(ApiState.Success(stateFlow.value)))
            fetchItems(
                from = currentPage * PAGE_SIZE,
                size = PAGE_SIZE
            ).map { apiState ->
                if (apiState is ApiState.Success) {
                    currentPage++
                    stateFlow.value = cachedItems.combineExistingListWithResult(
                        apiState.result as List<RecipeDto.Recipe>
                    )
                    PagingState.Content(ApiState.Success(stateFlow.value))
                } else {
                    PagingState.Content(apiState)
                }
            }.collect {
                emit(it)
            }
        }
    }

    private fun error(apiState: ApiState<List<RecipeDto.Recipe>>): ApiState.Error {
        val err = apiState as ApiState.Error
        return ApiState.Error(err.httpResult, err.code, err.message)
    }

    companion object {
        const val PAGE_SIZE = 10
        const val PARAM_FROM = "from"
        const val PARAM_SIZE = "size"
    }

    override fun fetchItemsFromNetwork(
        from: Int,
        size: Int
    ): Flow<ApiState<List<RecipeDto.Recipe>>> {
        return flow {
            val result = safeApiCall {
                val params = mapOf(
                    PARAM_FROM to from.toString(),
                    PARAM_SIZE to size.toString()
                )
                delay(2000)
                fakeRecipeService.getRecipes(from, size).results
            }
            emit(result)
        }
    }

    override fun fetchItemsFromDatabase(from: Int, size: Int): Flow<List<RecipeDto.Recipe>> = flow {
        emit(recipesDao.getRecipes(from, size).toRecipeList())
    }

    @Suppress("UNCHECKED_CAST")
    override fun saveItemsToDatabase(itemsList: List<Any>, refresh: Boolean) {
        recipesDao.insertOrUpdateWithRowIndex(
            (itemsList as List<RecipeDto.Recipe>).toRecipeEntityList(),
            currentPage,
            refresh
        )
    }

    fun getAllRecipes(): Flow<List<RecipeEntity>> = recipesDao.getAllRecipes()

}
