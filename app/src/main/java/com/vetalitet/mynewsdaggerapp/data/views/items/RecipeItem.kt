package com.vetalitet.mynewsdaggerapp.data.views.items

import com.vetalitet.mynewsdaggerapp.data.domain.Nutrition
import com.vetalitet.mynewsdaggerapp.data.domain.Section
import com.vetalitet.mynewsdaggerapp.data.domain.UserRatings

data class RecipeItem(
    val id: Int,
    val name: String?,
    val aspectRatio: String?,
    val thumbnail: String?,
    val createdAt: Long?,
    val sections: List<Section> = emptyList(),
    val rating: UserRatings? = null,
    val nutrition: Nutrition? = null
) : DataItem {
    override val itemId: Int = id

    var ingredientCount = sections.sumBy { section -> section.components.size }

}
