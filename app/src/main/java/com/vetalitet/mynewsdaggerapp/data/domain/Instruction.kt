package com.vetalitet.mynewsdaggerapp.data.domain

data class Instruction(
    val displayText: String,
    val id: Int,
    val position: Int
)