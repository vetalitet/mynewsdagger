package com.vetalitet.mynewsdaggerapp.data.views.items

interface DataItem {
    val itemId: Int
}