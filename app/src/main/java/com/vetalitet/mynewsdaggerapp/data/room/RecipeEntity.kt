package com.vetalitet.mynewsdaggerapp.data.room

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.vetalitet.mynewsdaggerapp.data.domain.Instruction
import com.vetalitet.mynewsdaggerapp.data.room.converters.InstructionConverter

@Entity(tableName = "recipes")
data class RecipeEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val name: String,
    val thumbnailUrl: String,
    val aspectRatio: String,
    val createdAt: Long,
    @TypeConverters(InstructionConverter::class)
    val instructions: List<Instruction>,
    val isFavourite: Boolean,

    var rowIndex: Int = 0
)
