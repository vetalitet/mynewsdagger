package com.vetalitet.mynewsdaggerapp.data.data

import javax.inject.Inject

class FavouriteRecipesRepository @Inject constructor(
    private val dataSource: FavouriteRecipesDataSource
) {

    fun initialLoad() = dataSource.initialLoad()

    fun loadMore(index: Int) = dataSource.loadMore(index)

}
