package com.vetalitet.mynewsdaggerapp.data.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vetalitet.mynewsdaggerapp.data.domain.Instruction

class InstructionConverter {

    private val gson: Gson by lazy { Gson() }

    @TypeConverter
    fun toInteractions(value: String?): List<Instruction>? {
        if (value == null) return null
        val listType = object : TypeToken<List<Instruction>>() { }.type
        return gson.fromJson<List<Instruction>>(value, listType)
    }

    @TypeConverter
    fun toString(value: List<Instruction>?): String? {
        if (value == null) return null
        return gson.toJson(value)
    }

}
