package com.vetalitet.mynewsdaggerapp.data.domain

data class Ingredient(
    val id: Int,
    val name: String
)
