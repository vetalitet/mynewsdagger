package com.vetalitet.mynewsdaggerapp.data.data

import com.vetalitet.mynewsdaggerapp.data.fake.FakeRecipeService
import com.vetalitet.mynewsdaggerapp.data.fake.RecipesServiceDao
import com.vetalitet.mynewsdaggerapp.data.room.RecipeEntity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@FlowPreview
@ExperimentalCoroutinesApi
class RecipesRepository @Inject constructor(
    private val recipesServiceDao: RecipesServiceDao,
    private val recipesDataSource: RecipesDataSource
) {

    fun initialLoad() = recipesDataSource.initialLoad()

    fun loadMore(index: Int) = recipesDataSource.loadMore(index)

    fun getAllRecipes(): Flow<List<RecipeEntity>> = recipesDataSource.getAllRecipes()

    // fake test data

    fun initServiceDao() = recipesServiceDao.insert(FakeRecipeService.items)

}
