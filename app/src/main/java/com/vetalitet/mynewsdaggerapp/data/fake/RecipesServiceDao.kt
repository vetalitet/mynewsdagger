package com.vetalitet.mynewsdaggerapp.data.fake

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface RecipesServiceDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(entities: List<RecipeServiceEntity>): List<Long>

    @Query("SELECT * FROM recipes_service ORDER BY rowIndex")
    fun getAll(): List<RecipeServiceEntity>

}
