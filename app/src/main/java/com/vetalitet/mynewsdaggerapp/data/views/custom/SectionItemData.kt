package com.vetalitet.mynewsdaggerapp.data.views.custom

data class SectionItemData(val index: Int, val text: String)
