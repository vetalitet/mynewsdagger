package com.vetalitet.mynewsdaggerapp.data.fake

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.vetalitet.mynewsdaggerapp.data.domain.Instruction
import com.vetalitet.mynewsdaggerapp.data.room.converters.InstructionConverter

@Entity(tableName = "recipes_service")
data class RecipeServiceEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val name: String,
    val thumbnailUrl: String,
    val aspectRatio: String = "",
    val createdAt: Long = 0,
    @TypeConverters(InstructionConverter::class)
    val instructions: List<Instruction> = emptyList(),
    val isFavourite: Boolean = false,

    var rowIndex: Int = 100
)
