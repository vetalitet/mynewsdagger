package com.vetalitet.mynewsdaggerapp.data.fake

import com.vetalitet.mynewsdaggerapp.network.model.RecipeDto

class FakeRecipeService(private val recipesServiceDao: RecipesServiceDao) {

    fun getRecipes(from: Int, size: Int): RecipeDto {
        return RecipeDto(results = recipesServiceDao.getAll().subList(from, from + size).toRecipeList())
    }

    companion object {
        val items = listOf(
            RecipeServiceEntity(36, "Recipe 36", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg", rowIndex = 1),
            RecipeServiceEntity(16, "Recipe 16", "https://i.picsum.photos/id/169/200/200.jpg?hmac=MquoCIcsCP_IxfteFmd8LfVF7NCoRre282nO9gVD0Yc", rowIndex = 2),
            RecipeServiceEntity(35, "Recipe 35", "https://i.picsum.photos/id/480/200/200.jpg?hmac=q_kzh_8Ih85_5t_jN3rcD3npeNBLA41oDGtQZVkmmYs", rowIndex = 3),
            RecipeServiceEntity(48, "Recipe 48", "https://i.picsum.photos/id/295/200/200.jpg?hmac=nsWHMt5f11TALPFeS_0t6tIlO2CkViBNAbAbSlhu8P4", rowIndex = 4),
            RecipeServiceEntity(50, "Recipe 50", "https://i.picsum.photos/id/508/200/200.jpg?hmac=K4JUehX1v2yEPLUOyJDAmRhZu8PgMu4vv6ypO-CA5nw", rowIndex = 5),
            RecipeServiceEntity(81, "Recipe 81", "https://i.picsum.photos/id/559/200/200.jpg?hmac=YMqBxDHO4-upCRoX_Ho2FNQg40ANP2MndFXD8sPsSAc", rowIndex = 6),
            RecipeServiceEntity(54, "Recipe 54", "https://i.picsum.photos/id/803/200/200.jpg?hmac=bVMK2xFyXtTrgCuUIozDz-mYxDZuDYFh-C_1LHZDKL4", rowIndex = 7),
            RecipeServiceEntity(89, "Recipe 89", "https://i.picsum.photos/id/144/200/200.jpg?hmac=3uevqKoBuYGJxqInvMh1R9bfnV2bz-Vetuv5Zwnb3mE", rowIndex = 8),
            RecipeServiceEntity(99, "Recipe 99", "https://i.picsum.photos/id/1016/200/200.jpg?hmac=VXVyuNaCgLl1UAdVez4gIo7AzMowZxMZVlIKlHMjgBw", rowIndex = 9),
            RecipeServiceEntity(71, "Recipe 71", "https://i.picsum.photos/id/430/200/200.jpg?hmac=RbYQ27bVLRKt5ScfTYiQ_ePoVdo70X4eWg2KPc6JF0I", rowIndex = 10),

            RecipeServiceEntity(18, "Recipe 18", "https://i.picsum.photos/id/440/200/200.jpg?hmac=KgupVeawJx9jCsstx3Ei3_HPctuUXH5wRAj9paxZ41U", rowIndex = 11),
            RecipeServiceEntity(69, "Recipe 69", "https://i.picsum.photos/id/638/200/200.jpg?hmac=64UpQ4ouFUNEG9cnXLQ9GxchDShg-mL1rdCrZGfc94U", rowIndex = 12),
            RecipeServiceEntity(49, "Recipe 49", "https://i.picsum.photos/id/667/200/200.jpg?hmac=Dqc51PnEPXpiiStRcDoPxytal0MOGvzg-eDZ4BsVIz8", rowIndex = 13),
            RecipeServiceEntity(61, "Recipe 61", "https://i.picsum.photos/id/1069/200/200.jpg?hmac=kdrhLJLz2Y5ldVFXX84RRoiGMO3j6z6AHWfWK7sxgI8", rowIndex = 14),
            RecipeServiceEntity(7, "Recipe 7", "https://i.picsum.photos/id/197/200/200.jpg?hmac=QpHQ9OiY_-qagHPzHZgTw7I_nE3LevYjH_1k3-xLpPk", rowIndex = 15),
            RecipeServiceEntity(39, "Recipe 39", "https://i.picsum.photos/id/607/200/200.jpg?hmac=ULikjE_L4XAS018UC9F2dCEiYIPKAFUW8oBDI2LzduY", rowIndex = 16),
            RecipeServiceEntity(38, "Recipe 38", "https://i.picsum.photos/id/976/200/200.jpg?hmac=xz9CTpScnLHQm_wNTcJmz8bQM6-ApTQnof5-4LGtu-s", rowIndex = 17),
            RecipeServiceEntity(30, "Recipe 30", "https://i.picsum.photos/id/128/200/200.jpg?hmac=m4AGhjLVIqDjy9qPNhbZyp8Gm_K03UAgP3IZnItJMA4", rowIndex = 18),
            RecipeServiceEntity(62, "Recipe 62", "https://i.picsum.photos/id/436/200/200.jpg?hmac=axiGy-zt6-TD5Hu1AD_rhudOgkfr-VQElZPKE592Mwc", rowIndex = 19),
            RecipeServiceEntity(1, "Recipe 1", "https://i.picsum.photos/id/1070/200/200.jpg?hmac=ulNtCwg9etYpYD_RxTGBFNLAbCqxJ0cj1L0WI7Ezcr4", rowIndex = 20),

            RecipeServiceEntity(70, "Recipe 70", "https://i.picsum.photos/id/147/200/200.jpg?hmac=Wk9sBW0wWCdCCp0HIczINX1TUZ_paAR_tCIKXcuYf2k", rowIndex = 21),
            RecipeServiceEntity(53, "Recipe 53", "https://i.picsum.photos/id/40/200/200.jpg?hmac=xkvWvgGjMuaPySCsshiYpLBOaphxinRhPkMRgx-LIYQ", rowIndex = 22),
            RecipeServiceEntity(51, "Recipe 51", "https://i.picsum.photos/id/599/200/200.jpg?hmac=2WLKs3sxIsaEQ-6WZaa6YMxgl6ZC4cNnid0aqupm2is", rowIndex = 23),
            RecipeServiceEntity(2, "Recipe 2", "https://i.picsum.photos/id/231/200/200.jpg?hmac=lUSm6Na5VxIhLKub6Y3JaBOAwCjkimAi-zHEOInwL58", rowIndex = 24),
            RecipeServiceEntity(66, "Recipe 66", "https://i.picsum.photos/id/654/200/200.jpg?hmac=oeNFLFRnWkWWnkuzoPKkwuIXUfPbCtvjpOq09MC1DrM", rowIndex = 25),
            RecipeServiceEntity(43, "Recipe 43", "https://i.picsum.photos/id/705/200/200.jpg?hmac=2HZlwayMAMOyCllDpM-Mx3u2Xyk40VRHAzlpNLKyTC8", rowIndex = 26),
            RecipeServiceEntity(3, "Recipe 3", "https://i.picsum.photos/id/852/200/200.jpg?hmac=4UHLpiS9j3YDnvq-w-MqnP5-ymiyvMs6BNV5ukoTRrI", rowIndex = 27),
            RecipeServiceEntity(13, "Recipe 13", "https://i.picsum.photos/id/154/200/200.jpg?hmac=ljiYfN3Am3TR0-aMErtWNuSQm8RTYarrv2QJfmWG6Cw", rowIndex = 28),
            RecipeServiceEntity(52, "Recipe 52", "https://i.picsum.photos/id/598/200/200.jpg?hmac=CGTNWD3Wfl8FFUMGok-Kj_SsE7Yc80U-jxup04hpB5k", rowIndex = 29),
            RecipeServiceEntity(56, "Recipe 56", "https://i.picsum.photos/id/960/200/200.jpg?hmac=jBtZLcx2FwawGC7rwl0dNWTD3q1uuB7CjJmALIF9pIg", rowIndex = 30),

            RecipeServiceEntity(72, "Recipe 72", "https://i.picsum.photos/id/10/200/200.jpg?hmac=Pal2P4G4LRZVjNnjESvYwti2SuEi-LJQqUKkQUoZq_g", rowIndex = 31),
            RecipeServiceEntity(21, "Recipe 21", "https://i.picsum.photos/id/842/200/200.jpg?hmac=RW9iEgAYLKwoinQWSz_zrZHyOwmVEgqvoZTPebkRGMM", rowIndex = 32),
            RecipeServiceEntity(29, "Recipe 29", "https://i.picsum.photos/id/404/200/200.jpg?hmac=7TesL9jR4uM2T_rW-vLbBjqvfeR37MJKTYA4TV-giwo", rowIndex = 33),
            RecipeServiceEntity(41, "Recipe 41", "https://i.picsum.photos/id/856/200/200.jpg?hmac=0i-cnhrzdHoF0g75vH6s_S8RUe5ej3nWdKiqxfYhuTo", rowIndex = 34),
            RecipeServiceEntity(34, "Recipe 34", "https://i.picsum.photos/id/988/200/200.jpg?hmac=-lwK-i6PssD9WlUeVPDIhOxDVxlzJKeM4MgEx_fIqJg", rowIndex = 35),
            RecipeServiceEntity(90, "Recipe 90", "https://i.picsum.photos/id/18/200/200.jpg?hmac=naWL3P7tSw9NeN2OXqD0XhBgnBko_h5B-Z3UdUVLFcU", rowIndex = 36),
            RecipeServiceEntity(73, "Recipe 73", "https://i.picsum.photos/id/683/200/200.jpg?hmac=gsOZBaeY42qvlTQSCuucn40FRUEnTdDYKl9q-YMcZh4", rowIndex = 37),
            RecipeServiceEntity(67, "Recipe 67", "https://i.picsum.photos/id/317/200/200.jpg?hmac=YE2w8fnOodl9AG9z8fKPtgEXGkIwnVxXXQVC6VCLJl8", rowIndex = 38),
            RecipeServiceEntity(79, "Recipe 79", "https://i.picsum.photos/id/823/200/200.jpg?hmac=zD0Ti1kYqMOUsfNVS7xtDou-2ECcI0RXYs18C54EdYo", rowIndex = 39),
            RecipeServiceEntity(24, "Recipe 24", "https://i.picsum.photos/id/609/200/200.jpg?hmac=Fe1MOOyr8RJSloodBDzO2oMGF2NvnJVhuoGNDTC5clU", rowIndex = 40),

            RecipeServiceEntity(23, "Recipe 23", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(47, "Recipe 47", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(28, "Recipe 28", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(85, "Recipe 85", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(98, "Recipe 98", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(32, "Recipe 32", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(76, "Recipe 76", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(74, "Recipe 74", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(10, "Recipe 10", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(37, "Recipe 37", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(22, "Recipe 22", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(77, "Recipe 77", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(57, "Recipe 57", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(8, "Recipe 8", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(86, "Recipe 86", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(59, "Recipe 59", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(97, "Recipe 97", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(15, "Recipe 15", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(5, "Recipe 5", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(95, "Recipe 95", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(11, "Recipe 11", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(33, "Recipe 33", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(63, "Recipe 63", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(19, "Recipe 19", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(83, "Recipe 83", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(27, "Recipe 27", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(94, "Recipe 94", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(68, "Recipe 68", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(26, "Recipe 26", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(58, "Recipe 58", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(4, "Recipe 4", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(55, "Recipe 55", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(93, "Recipe 93", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(12, "Recipe 12", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(6, "Recipe 6", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(17, "Recipe 17", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(87, "Recipe 87", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(44, "Recipe 44", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(45, "Recipe 45", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(78, "Recipe 78", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(14, "Recipe 14", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(40, "Recipe 40", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(84, "Recipe 84", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(64, "Recipe 64", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(88, "Recipe 88", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(9, "Recipe 9", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(82, "Recipe 82", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(80, "Recipe 80", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(31, "Recipe 31", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(65, "Recipe 65", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(91, "Recipe 91", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(42, "Recipe 42", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(46, "Recipe 46", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(25, "Recipe 25", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(75, "Recipe 75", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(60, "Recipe 60", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(20, "Recipe 20", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(92, "Recipe 92", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
            RecipeServiceEntity(96, "Recipe 96", "https://assets-global.website-files.com/6005fac27a49a9cd477afb63/6057684e5923ad2ae43c8150_bavassano_homepage_before.jpg"),
        )
    }
    
}

private fun List<RecipeServiceEntity>.toRecipeList(): List<RecipeDto.Recipe> {
    return this.map { it.toRecipe() }
}

private fun RecipeServiceEntity.toRecipe(): RecipeDto.Recipe {
    return RecipeDto.Recipe(
        id = this.id,
        name = this.name,
        thumbnailUrl = this.thumbnailUrl,
        createdAt = this.createdAt,
        aspectRatio = this.aspectRatio
    )
}
