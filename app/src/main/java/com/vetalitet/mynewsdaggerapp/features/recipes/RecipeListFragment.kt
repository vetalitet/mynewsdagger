package com.vetalitet.mynewsdaggerapp.features.recipes

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.WorkManager
import com.vetalitet.mynewsdaggerapp.BaseApplication
import com.vetalitet.mynewsdaggerapp.R
import com.vetalitet.mynewsdaggerapp.data.views.items.DataItem
import com.vetalitet.mynewsdaggerapp.data.views.items.RecipeItem
import com.vetalitet.mynewsdaggerapp.ui.adapters.RecipeListAdapter
import com.vetalitet.mynewsdaggerapp.ui.states.UiState
import com.vetalitet.mynewsdaggerapp.utils.isNetworkAvailable
import com.vetalitet.mynewsdaggerapp.utils.isPortraitMode
import com.vetalitet.mynewsdaggerapp.utils.toast
import kotlinx.android.synthetic.main.fragment_recipe_list.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

@FlowPreview
@ExperimentalCoroutinesApi
class RecipeListFragment : Fragment(R.layout.fragment_recipe_list) {

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private val viewModel: RecipeListViewModel by activityViewModels { factory }

    private val isNetworkAvailable: Boolean
        get() = requireContext().isNetworkAvailable()

    private val adapter by lazy {
        RecipeListAdapter(
            onItemClickListener = { dataItem, viewList ->
                viewModel.selectItem(dataItem.itemId)
                navigateToDetails(dataItem, viewList)
            },
            onReadyToLoadMore = { itemPosition ->
                viewModel.tryToLoadMore(itemPosition)
            }
        )
    }

    private fun navigateToDetails(dataItem: DataItem, viewList: List<View>) {
        (dataItem is RecipeItem).run {
            val item = dataItem as RecipeItem
            val extras = FragmentNavigatorExtras(*viewList.map {
                it to it.transitionName
            }.toTypedArray())

            val bundle = Bundle()
            bundle.putString("uri", item.thumbnail)
            bundle.putString("title", item.name)
            bundle.putString("aspectRatio", item.aspectRatio)

            findNavController().navigate(R.id.action_listFragment_to_detailsFragment, bundle, null, extras)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity().application as BaseApplication).coreComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSwipeRefreshLayout()
        intRecyclerView()
        initObservers()
        viewModel.initData(/*isNetworkAvailable*/)
        viewModel.launchUpdateWorker().observe(viewLifecycleOwner) { request ->
            WorkManager.getInstance(requireContext()).enqueue(request)
        }
    }

    private fun initSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener {
            viewModel.refreshData(isNetworkAvailable)
        }
    }

    private fun initObservers() {
        viewModel.initialLoadRecipesLiveData.observe(viewLifecycleOwner, { uiState ->
            shimmerLayout.isVisible = uiState is UiState.Loading
            if (uiState is UiState.Success) {
                adapter.items = uiState.result
                /*Handler(Looper.myLooper()!!).postDelayed({
                    recyclerView.smoothScrollToPosition(0)
                }, 100)*/
            } else if (uiState is UiState.Error) {
                uiState.message?.let { message -> requireContext().toast(message) }
            }
        })
        viewModel.loadMoreRecipesLiveData.observe(viewLifecycleOwner, { uiState ->
            when (uiState) {
                is UiState.Loading -> {
                    adapter.items = uiState.result
                }
                is UiState.Success -> {
                    adapter.items = uiState.result
                }
                is UiState.Error -> {
                    uiState.message?.let { message -> requireContext().toast(message) }
                }
            }
        })
    }

    private fun intRecyclerView() {
        val linearLayout = if (requireContext().isPortraitMode()) {
            LinearLayoutManager(requireContext())
        } else {
            GridLayoutManager(requireContext(), 2)
        }

        recyclerView.layoutManager = linearLayout
        recyclerView.addItemDecoration(
            DividerItemDecoration(context, linearLayout.orientation)
        )
        recyclerView.adapter = adapter

        postponeEnterTransition()
        recyclerView.viewTreeObserver.addOnPreDrawListener {
            startPostponedEnterTransition()
            true
        }
    }
}
