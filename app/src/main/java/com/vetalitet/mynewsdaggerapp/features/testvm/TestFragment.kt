package com.vetalitet.mynewsdaggerapp.features.testvm

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.vetalitet.mynewsdaggerapp.BaseApplication
import com.vetalitet.mynewsdaggerapp.R
import com.vetalitet.mynewsdaggerapp.ui.states.UiState
import kotlinx.android.synthetic.main.fragment_test.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

@ExperimentalCoroutinesApi
@FlowPreview
class TestFragment : Fragment(R.layout.fragment_test) {

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private val viewModel: TestViewModel by viewModels { factory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity().application as BaseApplication).coreComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        viewModel.itemsLiveData.observe(viewLifecycleOwner) {
            when (it) {
                is UiState.Loading -> tvTest.text = "Loading..."
                is UiState.Success -> tvTest.text = (it as UiState.Success).result?.size.toString()
                is UiState.Error -> tvTest.text = "Error!!!"
            }
        }
    }

    private fun initView() {
        btnRun.setOnClickListener {
            viewModel.loadInitialData()
        }
    }

}
