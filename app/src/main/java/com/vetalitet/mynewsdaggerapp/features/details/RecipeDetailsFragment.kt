package com.vetalitet.mynewsdaggerapp.features.details

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.vetalitet.mynewsdaggerapp.BaseApplication
import com.vetalitet.mynewsdaggerapp.R
import com.vetalitet.mynewsdaggerapp.data.domain.Instruction
import com.vetalitet.mynewsdaggerapp.data.domain.Recipe
import com.vetalitet.mynewsdaggerapp.data.domain.Section
import com.vetalitet.mynewsdaggerapp.data.mappers.toSectionItemData
import com.vetalitet.mynewsdaggerapp.data.views.custom.SectionItemData
import com.vetalitet.mynewsdaggerapp.features.recipes.RecipeListViewModel
import com.vetalitet.mynewsdaggerapp.ui.customviews.SectionItemView
import kotlinx.android.synthetic.main.fragment_recipe_details.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RecipeDetailsFragment : Fragment(R.layout.fragment_recipe_details) {

    private val args: RecipeDetailsFragmentArgs by navArgs()

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    @FlowPreview
    private val recipeListViewModel: RecipeListViewModel by activityViewModels { factory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity().application as BaseApplication).coreComponent.inject(this)
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(R.transition.shared_transition)
    }

    @FlowPreview
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        recipeListViewModel.selectedRecipe.observe(viewLifecycleOwner, { recipeOrNull ->
            recipeOrNull?.let { recipe ->
                showRecipe(recipe)
            }
        })
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()
        initData()
        initViews()
    }

    private fun initViews() {
        btnAddToFavourites.setOnClickListener {

        }
    }

    private fun initData() {
        ivRecipeImg.apply {
            transitionName = args.uri
            val (widthAspectRatio, heightAspectRatio) = extractRatioPair()
            widthRatio = widthAspectRatio
            heightRatio = heightAspectRatio
            Glide.with(this)
                .load(args.uri)
                .into(this)
        }
        collapsingToolbar.title = args.title
    }

    private fun extractRatioPair(): Pair<Int, Int> {
        val aspectRatio = args.aspectRatio?.split(":").handleRatio()
        val widthAspectRatio = aspectRatio[0]
        val heightAspectRatio = aspectRatio[1]
        return Pair(widthAspectRatio, heightAspectRatio)
    }

    private fun List<String>?.handleRatio(): List<Int> {
        return if (this == null || size < 2) {
            listOf(1, 1)
        } else {
            listOf(get(0).toInt(), get(1).toInt())
        }
    }

    private fun initToolbar() {
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(true)

        toolbar.setNavigationOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun showRecipe(recipe: Recipe) {
        showIngredients(recipe.sections)
        showInstructions(recipe.instructions)
    }

    private fun showInstructions(instructions: List<Instruction>) {
        instructions.forEach { instruction ->
            instructionsLayout.addView(sectionItemView(instruction.toSectionItemData()))
        }
    }

    private fun showIngredients(sections: List<Section>) {
        sections.forEach { section ->
            section.components.forEach { component ->
                ingredientsLayout.addView(sectionItemView(component.toSectionItemData()))
            }
        }
    }

    private fun sectionItemView(item: SectionItemData): View {
        val view = SectionItemView(requireContext())
        view.applyData(item)
        return view
    }

}
