package com.vetalitet.mynewsdaggerapp.features.favourites

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.vetalitet.mynewsdaggerapp.BaseApplication
import com.vetalitet.mynewsdaggerapp.R
import com.vetalitet.mynewsdaggerapp.features.recipes.RecipeListViewModel
import javax.inject.Inject

class FavouriteRecipesFragment: Fragment(R.layout.fragment_favourite_recipes) {

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private val viewModel: FavouriteRecipesViewModel by activityViewModels { factory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity().application as BaseApplication).coreComponent.inject(this)
    }

}
