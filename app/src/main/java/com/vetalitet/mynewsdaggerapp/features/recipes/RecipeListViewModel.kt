package com.vetalitet.mynewsdaggerapp.features.recipes

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.*
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkRequest
import com.vetalitet.mynewsdaggerapp.data.data.RecipesRepository
import com.vetalitet.mynewsdaggerapp.data.domain.Recipe
import com.vetalitet.mynewsdaggerapp.data.mappers.mapToDomainRecipesList
import com.vetalitet.mynewsdaggerapp.data.mappers.mapToResult
import com.vetalitet.mynewsdaggerapp.data.views.items.DataItem
import com.vetalitet.mynewsdaggerapp.data.views.items.ProgressItem
import com.vetalitet.mynewsdaggerapp.features.recipes.updater.RecipesUpdaterWorker
import com.vetalitet.mynewsdaggerapp.network.model.RecipeDto
import com.vetalitet.mynewsdaggerapp.network.states.ApiState
import com.vetalitet.mynewsdaggerapp.network.states.PagingState
import com.vetalitet.mynewsdaggerapp.ui.states.UiState
import com.vetalitet.mynewsdaggerapp.utils.SingleLiveEvent
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import kotlin.time.ExperimentalTime

@FlowPreview
@ExperimentalCoroutinesApi
class RecipeListViewModel @Inject constructor(
    private val recipesRepository: RecipesRepository,
    private val sharedPreferences: SharedPreferences,
    private val dispatcher: CoroutineDispatcher,
) : ViewModel() {

    private val _initialLoadRecipesLiveData = SingleLiveEvent<UiState<List<DataItem>>>()
    val initialLoadRecipesLiveData: LiveData<UiState<List<DataItem>>> = _initialLoadRecipesLiveData

    private val _initialA = SingleLiveEvent<Int>()
    val initialA: LiveData<Int> = _initialA

    private val _loadMoreRecipesLiveData = SingleLiveEvent<UiState<List<DataItem>>>()
    val loadMoreRecipesLiveData: LiveData<UiState<List<DataItem>>> = _loadMoreRecipesLiveData

    private val _selectedRecipe = MutableLiveData<Recipe?>()
    val selectedRecipe: LiveData<Recipe?> = _selectedRecipe

    private var items: List<Recipe> = mutableListOf()

    fun refreshData(isPortrait: Boolean) {
        viewModelScope.launch(dispatcher) {

        }
    }

    init {
        if (sharedPreferences.getBoolean("IS_FIRST_RUN", true)) {
            viewModelScope.launch {
                initServiceDao()
            }
            sharedPreferences.edit().putBoolean("IS_FIRST_RUN", false).apply()
        }

        viewModelScope.launch(dispatcher) {
            recipesRepository.getAllRecipes().collect {
                Log.d("DEBUG_DEBUG", "<=======================================>")
                it.forEach {
                    Log.d("DEBUG_DEBUG", "${it.id}  -  ${it.rowIndex}")
                }
            }
        }
    }

    fun launchUpdateWorker(): LiveData<WorkRequest> {
        val liveData = MutableLiveData<WorkRequest>()
        val request = OneTimeWorkRequestBuilder<RecipesUpdaterWorker>()
            //.setInputData(build)
            .setConstraints(
                Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build()
            )
            .build()
        liveData.value = request
        return liveData
    }

    @OptIn(ExperimentalTime::class)
    @FlowPreview
    fun initData() {
        viewModelScope.launch(dispatcher) {
            //if (items.isNotEmpty()) return@launch

            recipesRepository.initialLoad().map { pagingState ->
                if (pagingState is PagingState.Content) {
                    val apiState = pagingState.data
                    _initialLoadRecipesLiveData.postValue(getServiceValue(apiState))
                } else if (pagingState is PagingState.Initial) {
                    _initialLoadRecipesLiveData.postValue(UiState.Loading())
                }
            }.asLiveData()
        }
    }

    fun tryToLoadMore(index: Int, isPortrait: Boolean = true) {
        viewModelScope.launch {
            recipesRepository.loadMore(index).collect { pagingState ->
                if (pagingState is PagingState.Content) {
                    val apiState = pagingState.data
                    _loadMoreRecipesLiveData.postValue(getServiceValue(apiState))
                } else if (pagingState is PagingState.Paging) {
                    val apiState = pagingState.availableContent
                    val dataItems = getApiResult(apiState, isPortrait)
                    _loadMoreRecipesLiveData.postValue(UiState.Loading(dataItems))
                }
            }
        }
    }

    private fun getApiResult(
        apiState: ApiState<List<RecipeDto.Recipe>>,
        isPortrait: Boolean
    ): List<DataItem> {
        items = ((apiState as ApiState.Success).result?.mapToDomainRecipesList() ?: emptyList())
        val dataItems = items.mapToResult()
        (dataItems as MutableList).add(ProgressItem)
        if (!isPortrait) {
            dataItems.add(ProgressItem)
        }
        return dataItems
    }

    private fun getServiceValue(data: ApiState<List<RecipeDto.Recipe>>): UiState<List<DataItem>> {
        return when (data) {
            is ApiState.Success -> {
                items = (data.result?.mapToDomainRecipesList() ?: emptyList())
                UiState.Success(items.mapToResult())
            }
            is ApiState.Error -> {
                UiState.Error(data.message)
            }
        }
    }

    private fun findItemById(id: Int): Recipe? {
        return items.firstOrNull { it.id == id }
    }

    fun selectItem(itemId: Int) {
        val item = findItemById(itemId)
        _selectedRecipe.postValue(item)
    }

    private suspend fun initServiceDao() {
        withContext(Dispatchers.IO) {
            recipesRepository.initServiceDao()
        }
    }

}
