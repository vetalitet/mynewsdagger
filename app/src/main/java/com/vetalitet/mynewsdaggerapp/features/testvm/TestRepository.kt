package com.vetalitet.mynewsdaggerapp.features.testvm

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class TestRepository @Inject constructor() {

    fun getInitialData() = flow {
        delay(3000)
        emit(listOf("One", "Two", "Three"))
    }

}
