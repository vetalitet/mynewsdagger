package com.vetalitet.mynewsdaggerapp.features.favourites

import androidx.lifecycle.ViewModel
import com.vetalitet.mynewsdaggerapp.data.data.FavouriteRecipesRepository
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class FavouriteRecipesViewModel @Inject constructor(
    private val repository: FavouriteRecipesRepository,
    private val dispatcher: CoroutineDispatcher
): ViewModel() {

}
