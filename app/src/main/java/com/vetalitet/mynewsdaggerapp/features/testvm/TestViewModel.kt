package com.vetalitet.mynewsdaggerapp.features.testvm

import androidx.lifecycle.*
import com.vetalitet.mynewsdaggerapp.ui.states.UiState
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class TestViewModel @Inject constructor(
    private val testRepository: TestRepository,
    private val dispatcher: CoroutineDispatcher,
) : ViewModel() {

    private val _itemsLiveData: MutableLiveData<UiState<List<String>>> = MutableLiveData()
    val itemsLiveData: LiveData<UiState<List<String>>> = _itemsLiveData

    fun loadInitialData() {
        _itemsLiveData.postValue(UiState.Loading())
        viewModelScope.launch(dispatcher) {
            testRepository.getInitialData().collect {
                _itemsLiveData.postValue(UiState.Success(it))
            }
        }
    }

}
