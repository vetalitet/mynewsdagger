package com.vetalitet.mynewsdaggerapp.features.recipes.updater

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.vetalitet.mynewsdaggerapp.data.data.RecipesRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject
import javax.inject.Provider

@FlowPreview
class RecipesUpdaterWorker @ExperimentalCoroutinesApi constructor(
    private val context: Context,
    private val params: WorkerParameters,
    private val recipesRepository: RecipesRepository
) : Worker(context, params) {

    override fun doWork(): Result {
        Log.d("RecipesUpdaterWorker", "Repository: $recipesRepository")
        return Result.success()
    }

    @ExperimentalCoroutinesApi
    class Factory @Inject constructor(
        private val provider: Provider<RecipesRepository>
    ) : ChildWorkerFactory {
        override fun create(appContext: Context, params: WorkerParameters): ListenableWorker {
            return RecipesUpdaterWorker(
                appContext,
                params,
                provider.get()
            )
        }
    }

}
