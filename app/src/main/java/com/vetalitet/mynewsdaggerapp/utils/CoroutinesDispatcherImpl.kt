package com.vetalitet.mynewsdaggerapp.utils

import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class CoroutinesDispatcherImpl @Inject constructor(
    override val main: CoroutineDispatcher,
    override val computation: CoroutineDispatcher,
    override val io: CoroutineDispatcher,
    override val default: CoroutineDispatcher,
    override val singleThread: CoroutineDispatcher
) : CoroutineDispatcherProvider

interface CoroutineDispatcherProvider {
    val main: CoroutineDispatcher
    val computation: CoroutineDispatcher
    val io: CoroutineDispatcher
    val default: CoroutineDispatcher
    val singleThread: CoroutineDispatcher
}
