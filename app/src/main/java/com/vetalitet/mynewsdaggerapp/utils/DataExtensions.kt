package com.vetalitet.mynewsdaggerapp.utils

import com.vetalitet.mynewsdaggerapp.network.model.RecipeDto

fun List<RecipeDto.Recipe>.combineExistingListWithResult(
    apiState: List<RecipeDto.Recipe>
): List<RecipeDto.Recipe> {
    return (this.associateBy(RecipeDto.Recipe::id) + apiState.associateBy(RecipeDto.Recipe::id)).values.toList()
}
