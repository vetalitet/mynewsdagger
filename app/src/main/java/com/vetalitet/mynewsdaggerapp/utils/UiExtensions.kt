package com.vetalitet.mynewsdaggerapp.utils

import android.content.Context
import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat

fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Context.isPortraitMode() = resources.configuration.orientation == ORIENTATION_PORTRAIT

fun Context.color(@ColorRes color: Int) = ContextCompat.getColor(this, color)
