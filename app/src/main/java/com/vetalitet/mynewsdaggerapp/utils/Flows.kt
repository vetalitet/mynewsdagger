package com.vetalitet.mynewsdaggerapp.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.seconds

@ExperimentalTime
fun <T> Flow<T>.toLiveData(
    timeout: Duration = 5.seconds
): LiveData<T> = liveData(timeoutInMs = timeout.toLongMilliseconds()) {
    collect { emit(it) }
}

/*@UseExperimental(ExperimentalCoroutinesApi::class)
fun <T> LiveData<T>.asFlow(): Flow<T> = callbackFlow {
    val observer = Observer<T> { value -> offer(value) }
    observeForever(observer)
    awaitClose {
        removeObserver(observer)
    }
}.flowOn(Dispatchers.Main.immediate)
*/