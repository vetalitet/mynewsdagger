package com.vetalitet.mynewsdaggerapp.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.widget.FrameLayout
import android.widget.TextView
import com.vetalitet.mynewsdaggerapp.R

class RatingView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStylesAttr: Int = 0
) : FrameLayout(context, attrs, defStylesAttr) {

    init {
        inflate(context, R.layout.cv_rating, this)

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.RatingView)
        val rating = typedArray.getString(R.styleable.RatingView_android_text)
        typedArray.recycle()

        findViewById<TextView>(R.id.tvRating).text = rating.orEmpty().ifEmpty { "4,3" }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width = measuredWidth
        val height = measuredHeight

        val size = if (width > height) {
            height
        } else {
            width
        }
        setMeasuredDimension(size, size)
    }

}
