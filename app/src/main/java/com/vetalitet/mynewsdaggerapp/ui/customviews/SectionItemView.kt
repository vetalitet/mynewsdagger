package com.vetalitet.mynewsdaggerapp.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.vetalitet.mynewsdaggerapp.R
import com.vetalitet.mynewsdaggerapp.data.views.custom.SectionItemData
import kotlinx.android.synthetic.main.cv_section_item_view.view.*

class SectionItemView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStylesAttr: Int = 0
) : LinearLayout(context, attrs, defStylesAttr) {

    init {
        inflate(context, R.layout.cv_section_item_view, this)
    }

    fun applyData(item: SectionItemData) {
        tvMessage.text = String.format(
            resources.getString(R.string.index_message),
            item.index,
            item.text
        )
    }

}
