package com.vetalitet.mynewsdaggerapp.ui.customviews.favouritesbutton

import android.content.Context
import android.graphics.drawable.StateListDrawable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.vetalitet.mynewsdaggerapp.R

class AddToFavouritesButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr), CoordinatorLayout.AttachedBehavior,
    View.OnClickListener {

    companion object {
        private const val TAG = "AddToFavouritesButton"

        val STATE_DISLIKED = intArrayOf(R.attr.state_disliked)
        val STATE_LIKED = intArrayOf(R.attr.state_liked)
    }

    private val impl by lazy { AddToFavouritesButtonImpl(this) }

    var buttonState: ButtonState = ButtonState.Disliked
        set(value) {
            field = value
            refreshDrawableState()
        }

    init {
        setBackgroundTint(resources.getColor(R.color.colorPrimaryDark))

        val attributes = context.obtainStyledAttributes(
            attrs, R.styleable.AddToFavouritesButton, defStyleAttr, 0
        )

        var stateListDrawable: StateListDrawable? = null
        val drawable = attributes.getDrawable(R.styleable.AddToFavouritesButton_android_src)
        if (drawable == null) {
            val draw = ContextCompat.getDrawable(context, R.drawable.ic_heart_selector)
            if (draw != null) {
                stateListDrawable = draw as StateListDrawable
            }
        } else if (drawable is StateListDrawable) {
            stateListDrawable = drawable
        } else {
            throw IllegalArgumentException("AddToFavouritesButton needs a StateListDrawable to be used in android:src")
        }

        setImageDrawable(stateListDrawable)
        scaleType = ScaleType.CENTER_INSIDE

        attributes.recycle()

        setOnClickListener(this)
    }

    override fun onCreateDrawableState(extraSpace: Int): IntArray {
        val drawableState = super.onCreateDrawableState(extraSpace + 2)
        when (buttonState) {
            is ButtonState.Disliked -> mergeDrawableStates(drawableState, STATE_DISLIKED)
            is ButtonState.Liked -> mergeDrawableStates(drawableState, STATE_LIKED)
        }
        return drawableState
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                isPressed = true
            }
            MotionEvent.ACTION_UP -> {
                if (isPressed) {
                    performClick()
                    isPressed = false
                }
            }
        }
        return true
    }

    override fun onClick(v: View?) {
        if (buttonState == ButtonState.Disliked) {
            buttonState = ButtonState.Liked
        } else {
            buttonState = ButtonState.Disliked
        }
        buttonClickListener?.invoke(buttonState)
        refreshDrawableState()
    }

    var buttonClickListener: ((ButtonState) -> Unit)? = null

    fun setOnClickListener(state: (ButtonState) -> Unit) {
        buttonClickListener = state
    }

    fun setBackgroundTint(color: Int) {
        val drawable = ContextCompat.getDrawable(context, R.drawable.round_button_bg)
        background = drawable
        background?.let { DrawableCompat.setTint(it, color) }
    }

    fun show() {
        impl.show()
    }

    fun hide() {
        impl.hide()
    }

    override fun getBehavior(): CoordinatorLayout.Behavior<AddToFavouritesButton> {
        return AddToFavouritesButtonBehavior()
    }

    sealed class ButtonState {
        object Liked : ButtonState()
        object Disliked : ButtonState()
    }

}
