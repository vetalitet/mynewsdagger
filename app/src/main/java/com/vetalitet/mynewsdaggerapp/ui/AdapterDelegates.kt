package com.vetalitet.mynewsdaggerapp.ui

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.vetalitet.mynewsdaggerapp.R
import com.vetalitet.mynewsdaggerapp.data.views.items.AdvertisementItem
import com.vetalitet.mynewsdaggerapp.data.views.items.DataItem
import com.vetalitet.mynewsdaggerapp.data.views.items.ProgressItem
import com.vetalitet.mynewsdaggerapp.data.views.items.RecipeItem

object AdapterDelegates {

    fun recipesItemDelegate(
        itemCount: () -> Int,
        itemClickListener: (DataItem, List<View>) -> Unit,
        onReadyToLoadMore: (Int) -> Unit
    ) = adapterDelegate<RecipeItem, DataItem>(R.layout.item_small_news) {
        val tvViewGroup: ViewGroup = findViewById(R.id.rootView)
        val tvTitle: TextView = findViewById(R.id.tvTitle)
        val ivPicture: ImageView = findViewById(R.id.imageView)
        val tvCalories: TextView = findViewById(R.id.tvCalories)
        val tvIngredients: TextView = findViewById(R.id.tvIngredients)
        val tvRating: TextView = findViewById(R.id.tvRating)

        tvViewGroup.setOnClickListener { itemClickListener.invoke(item, listOf(ivPicture)) }

        bind { payloads ->
            tvTitle.text = item.name
            ivPicture.transitionName = item.thumbnail
            item.thumbnail?.let { imageUrl ->
                loadImage(ivPicture, imageUrl)
            }
            tvIngredients.text = item.ingredientCount.toString()
            tvCalories.text = item.nutrition?.calories?.toString() ?: "n/a"
            tvRating.text = item.rating?.score?.let { String.format("%.1f", it) } ?: "n/a"

            if (adapterPosition > 0 && adapterPosition + 1 == itemCount.invoke()) {
                onReadyToLoadMore(adapterPosition)
            }
        }
    }

    fun progressDelegate() =
        adapterDelegate<ProgressItem, DataItem>(R.layout.item_progress_shimmer) {}

    fun advertisementItemDelegate() =
        adapterDelegate<AdvertisementItem, DataItem>(R.layout.item_advertisement_news_data) {}

    private fun loadImage(imageView: ImageView, imageUrl: String) {
        val requestOptions = RequestOptions()
            .placeholder(R.drawable.ic_placeholder)
            .centerCrop()

        Glide.with(imageView.context)
            .load(imageUrl)
            .transition(withCrossFade())
            .apply(requestOptions)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(imageView)
    }

}
