package com.vetalitet.mynewsdaggerapp.ui.adapters

import android.view.View
import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import com.vetalitet.mynewsdaggerapp.data.views.items.DataItem
import com.vetalitet.mynewsdaggerapp.ui.AdapterDelegates

class RecipeListAdapter(
    val onItemClickListener: (DataItem, List<View>) -> Unit,
    val onReadyToLoadMore: (Int) -> Unit
) : AsyncListDifferDelegationAdapter<DataItem>(DiffUtilsCallback()) {

    init {
        delegatesManager.addDelegate(AdapterDelegates.progressDelegate())
        delegatesManager.addDelegate(
            AdapterDelegates.recipesItemDelegate(
                { itemCount },
                onItemClickListener,
                onReadyToLoadMore
            )
        )
        delegatesManager.addDelegate(AdapterDelegates.advertisementItemDelegate())
    }

    class DiffUtilsCallback : DiffUtil.ItemCallback<DataItem>() {
        override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem) =
            oldItem.itemId == newItem.itemId

        override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem) =
            oldItem.equals(newItem)
    }

}