package com.vetalitet.mynewsdaggerapp.ui.customviews

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.core.animation.doOnEnd

class TestCustomView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStylesAttr: Int = 0
) : View(context, attrs, defStylesAttr) {

    private var bars = emptyList<BarItem>()

    fun init(barsCount: Int) {
        Log.d("CUSTOM_VIEW", "init()")
        bars = (0 until barsCount).map { BarItem() }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        Log.d("CUSTOM_VIEW", "onDraw")
        bars.forEachIndexed { index, item ->
            Log.d("CUSTOM_VIEW", "Item $index: ${item.value}")
        }
        //invalidate()
    }

    fun updateBarsRelatively(data: List<Float>) {
        Log.d("CUSTOM_VIEW", "updateBarsRelatively")
        bars.forEachIndexed { index, item ->
            item.updateValueRelatively(data[index])
        }
    }

}

class BarItem() {
    private var _value = 0f
    val value: Float
        get() = _value

    fun updateValueRelatively(delta: Float) {
        ValueAnimator.ofFloat(0f, 1f).apply {
            duration = 500
            addUpdateListener {
                _value = it.animatedValue as Float
                Log.d("CUSTOM_VIEW", "Update: $_value")
                doOnEnd {
                    reverseInterpolator()
                }
            }

            start()
        }
    }

    private fun reverseInterpolator() {
        ValueAnimator.ofFloat(1f, 0f).apply {
            duration = 500
            addUpdateListener {
                _value = it.animatedValue as Float
                Log.d("CUSTOM_VIEW", "Update: $_value")
            }

            start()
        }
    }
}