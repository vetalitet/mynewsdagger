package com.vetalitet.mynewsdaggerapp.ui.customviews.favouritesbutton

import android.graphics.Rect
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.internal.DescendantOffsetUtils

class AddToFavouritesButtonBehavior : CoordinatorLayout.Behavior<AddToFavouritesButton>() {

    var tmpRect: Rect = Rect()

    override fun onDependentViewChanged(
        parent: CoordinatorLayout,
        child: AddToFavouritesButton,
        dependency: View
    ): Boolean {
        if (dependency is AppBarLayout) {
            updateFabVisibilityForAppBarLayout(parent, dependency, child)
        }
        return false
    }

    //noinspection RestrictedApi
    private fun updateFabVisibilityForAppBarLayout(
        parent: CoordinatorLayout,
        appBarLayout: AppBarLayout,
        child: AddToFavouritesButton
    ): Boolean {
        val rect: Rect = tmpRect
        DescendantOffsetUtils.getDescendantRect(parent, appBarLayout, rect)

        if (rect.bottom <= appBarLayout.minimumHeightForVisibleOverlappingContent) {
            child.hide()
        } else {
            child.show()
        }
        return true
    }

}
