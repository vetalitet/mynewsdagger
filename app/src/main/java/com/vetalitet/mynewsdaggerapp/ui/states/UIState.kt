package com.vetalitet.mynewsdaggerapp.ui.states

sealed class UiState<out T : Any> {
    data class Loading<out T : Any>(val result: T? = null) : UiState<T>()
    data class Error(val message: String? = null) : UiState<Nothing>()
    data class Success<out T : Any>(val result: T? = null) : UiState<T>()
}
