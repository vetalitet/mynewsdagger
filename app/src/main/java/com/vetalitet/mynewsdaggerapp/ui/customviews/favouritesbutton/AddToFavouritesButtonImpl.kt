package com.vetalitet.mynewsdaggerapp.ui.customviews.favouritesbutton

import android.animation.*
import android.graphics.Matrix
import android.graphics.Matrix.ScaleToFit
import android.graphics.RectF
import android.os.Build
import android.view.View
import androidx.core.view.ViewCompat
import com.google.android.material.animation.AnimatorSetCompat
import com.google.android.material.animation.ImageMatrixProperty
import com.google.android.material.animation.MatrixEvaluator
import com.google.android.material.animation.MotionSpec
import com.vetalitet.mynewsdaggerapp.R
import java.util.*

class AddToFavouritesButtonImpl(val view: AddToFavouritesButton) {

    companion object {
        const val TAG = "AddToFavouritesButtonIm"

        const val ANIM_STATE_NONE = 0
        const val ANIM_STATE_HIDING = 1
        const val ANIM_STATE_SHOWING = 2

        private const val HIDE_OPACITY = 0f
        private const val HIDE_SCALE = 0f
        private const val HIDE_ICON_SCALE = 0f
        private const val SHOW_OPACITY = 1f
        private const val SHOW_SCALE = 1f
        private const val SHOW_ICON_SCALE = 1f
    }

    private var currentAnimator: Animator? = null
    private var animState = ANIM_STATE_NONE

    private var imageMatrixScale = 1f
    private val maxImageSize = 0
    private val tmpMatrix = Matrix()

    private val tmpRectF1 = RectF()
    private val tmpRectF2 = RectF()

    fun show() {
        if (isOrWillBeShown()) {
            // We either are or will soon be visible, skip the call
            return
        }

        currentAnimator?.cancel()

        if (shouldAnimateVisibilityChange()) {
            if (view.visibility != View.VISIBLE) {
                // If the view isn't visible currently, we'll animate it from a single pixel
                view.alpha = 0f
                view.scaleY = 0f
                view.scaleX = 0f
                setImageMatrixScale(0f)
            }
            val spec =
                MotionSpec.createFromResource(view.context, R.animator.hide_motion_spec)!!
            val set: AnimatorSet = createAnimator(spec, SHOW_OPACITY, SHOW_SCALE, SHOW_ICON_SCALE)
            set.addListener(
                object : AnimatorListenerAdapter() {
                    override fun onAnimationStart(animation: Animator) {
                        view.visibility = View.VISIBLE
                        animState = ANIM_STATE_SHOWING
                        currentAnimator = animation
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        animState = ANIM_STATE_NONE
                        currentAnimator = null
                    }
                })
            set.start()
        }
    }

    fun hide() {
        if (isOrWillBeHidden()) {
            // We either are or will soon be hidden, skip the call
            return
        }

        currentAnimator?.cancel()

        if (shouldAnimateVisibilityChange()) {
            val spec =
                MotionSpec.createFromResource(view.context, R.animator.hide_motion_spec)!!
            val set: AnimatorSet = createAnimator(spec, HIDE_OPACITY, HIDE_SCALE, HIDE_ICON_SCALE)
            set.addListener(
                object : AnimatorListenerAdapter() {
                    private var cancelled = false
                    override fun onAnimationStart(animation: Animator) {
                        view.visibility = View.VISIBLE
                        animState = ANIM_STATE_HIDING
                        currentAnimator = animation
                        cancelled = false
                    }

                    override fun onAnimationCancel(animation: Animator) {
                        cancelled = true
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        animState = ANIM_STATE_NONE
                        currentAnimator = null
                        if (!cancelled) {
                            view.visibility = View.INVISIBLE
                        }
                    }
                })
            set.start()
        }
    }

    private fun setImageMatrixScale(scale: Float) {
        imageMatrixScale = scale
        val matrix = tmpMatrix
        calculateImageMatrixFromScale(scale, matrix)
        view.imageMatrix = matrix
    }

    private fun isOrWillBeHidden(): Boolean {
        return if (view.visibility == View.VISIBLE) {
            // If we currently visible, return true if we're animating to be hidden
            animState == ANIM_STATE_HIDING
        } else {
            // Otherwise if we're not visible, return true if we're not animating to be shown
            animState != ANIM_STATE_SHOWING
        }
    }

    fun isOrWillBeShown(): Boolean {
        return if (view.visibility != View.VISIBLE) {
            // If we not currently visible, return true if we're animating to be shown
            animState == ANIM_STATE_SHOWING
        } else {
            // Otherwise if we're visible, return true if we're not animating to be hidden
            animState != ANIM_STATE_HIDING
        }
    }

    private fun shouldAnimateVisibilityChange(): Boolean {
        return ViewCompat.isLaidOut(view) && !view.isInEditMode
    }

    private fun createAnimator(
        spec: MotionSpec, opacity: Float, scale: Float, iconScale: Float
    ): AnimatorSet {
        val animators: MutableList<Animator> = ArrayList()

        val animatorOpacity = ObjectAnimator.ofFloat(view, View.ALPHA, opacity)
        spec.getTiming("opacity").apply(animatorOpacity)
        animators.add(animatorOpacity)

        val animatorScaleX = ObjectAnimator.ofFloat(view, View.SCALE_X, scale)
        spec.getTiming("scale").apply(animatorScaleX)
        workAroundOreoBug(animatorScaleX)
        animators.add(animatorScaleX)

        val animatorScaleY = ObjectAnimator.ofFloat(view, View.SCALE_Y, scale)
        spec.getTiming("scale").apply(animatorScaleY)
        workAroundOreoBug(animatorScaleY)
        animators.add(animatorScaleY)

        calculateImageMatrixFromScale(iconScale, tmpMatrix)
        val animatorIconScale = ObjectAnimator.ofObject(
            view,
            ImageMatrixProperty(),
            object : MatrixEvaluator() {
                override fun evaluate(
                    fraction: Float, startValue: Matrix, endValue: Matrix
                ): Matrix {
                    // Also set the current imageMatrixScale fraction so it can be used to correctly
                    // calculate the image matrix at any given point.
                    imageMatrixScale = fraction
                    return super.evaluate(fraction, startValue, endValue)
                }
            },
            Matrix(tmpMatrix)
        )
        spec.getTiming("iconScale").apply(animatorIconScale)
        animators.add(animatorIconScale)
        val set = AnimatorSet()
        AnimatorSetCompat.playTogether(set, animators)
        return set
    }

    /**
     * There appears to be a bug in the OpenGL shadow rendering code on API 26. We can work around it
     * by preventing any scaling close to 0.
     */
    private fun workAroundOreoBug(animator: ObjectAnimator) {
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.O) {
            return
        }
        animator.setEvaluator(object : TypeEvaluator<Float?> {
            var floatEvaluator = FloatEvaluator()
            override fun evaluate(fraction: Float, startValue: Float?, endValue: Float?): Float? {
                val evaluated = floatEvaluator.evaluate(fraction, startValue, endValue)
                return if (evaluated < 0.1f) 0.0f else evaluated
            }
        })
    }

    private fun calculateImageMatrixFromScale(scale: Float, matrix: Matrix) {
        matrix.reset()
        val drawable = view.drawable
        if (drawable != null && maxImageSize != 0) {
            // First make sure our image respects mMaxImageSize.
            val drawableBounds: RectF = tmpRectF1
            val imageBounds: RectF = tmpRectF2
            drawableBounds[0f, 0f, drawable.intrinsicWidth.toFloat()] =
                drawable.intrinsicHeight.toFloat()
            imageBounds[0f, 0f, maxImageSize.toFloat()] = maxImageSize.toFloat()
            matrix.setRectToRect(drawableBounds, imageBounds, ScaleToFit.CENTER)

            // Then scale it as requested.
            matrix.postScale(scale, scale, maxImageSize / 2f, maxImageSize / 2f)
        }
    }

}
