package com.vetalitet.mynewsdaggerapp.network.states

sealed class PagingState<out T> {
    // state, on which we don't have anything. It's before any data loaded
    object Initial: PagingState<Nothing>()

    // state, when first data is loaded
    data class Content<T>(val data: T): PagingState<T>()

    data class Paging<T>(val availableContent: T): PagingState<T>()
}
