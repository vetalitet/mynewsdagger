package com.vetalitet.mynewsdaggerapp.network.model

import com.google.gson.annotations.SerializedName

data class NewsDto(
    @SerializedName("articles")
    val articles: List<Article>? = null
) {
    data class Article(
        @SerializedName("title") val title: String? = null,
        @SerializedName("url") val url: String? = null,
        @SerializedName("urlToImage") val urlToImage: String? = null
    )
}
