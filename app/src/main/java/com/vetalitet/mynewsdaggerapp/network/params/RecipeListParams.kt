package com.vetalitet.mynewsdaggerapp.network.params

data class RecipeListParams(
    val tags: String = "under_30_minutes"
) {
    fun toMap(): Map<String, String> = mutableMapOf<String, String>().apply {
        put("tags", tags)
    }
}
