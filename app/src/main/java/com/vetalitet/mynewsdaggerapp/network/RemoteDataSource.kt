package com.vetalitet.mynewsdaggerapp.network

import com.vetalitet.mynewsdaggerapp.network.HttpResult.*
import com.vetalitet.mynewsdaggerapp.network.states.ApiState
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

open class RemoteDataSource {

    companion object {

        suspend fun <T> safeApiCall(
            apiCall: suspend () -> T
        ): ApiState<T> {
            return try {
                ApiState.Success(apiCall.invoke())
            } catch (throwable: Throwable) {
                when (throwable) {
                    is HttpException -> {
                        val result = when (throwable.code()) {
                            in 500..599 -> {
                                val message = "Server error: ${throwable.localizedMessage}"
                                ApiState.Error(HttpResult.SERVER_ERROR, throwable.code(), message)
                            }
                            429 -> {
                                error(
                                    MONTH_CONNECTIONS_LIMIT,
                                    throwable.code(),
                                    "The MONTHLY quota for Requests reached!"
                                )
                            }
                            else -> error(NOT_DEFINED, throwable.code(), "Undefined error")
                        }
                        result
                    }
                    is SocketTimeoutException -> error(NO_CONNECTION, null, "Slow connection")
                    is UnknownHostException -> error(TIMEOUT, null, "No internet connection")
                    is IOException -> error(BAD_RESPONSE, null, throwable.localizedMessage)
                    else -> ApiState.Error(NOT_DEFINED, null, throwable.localizedMessage)
                }
            }
        }

        private fun error(cause: HttpResult, code: Int?, errorMessage: String?): ApiState.Error {
            return ApiState.Error(cause, code, errorMessage)
        }

    }

}
