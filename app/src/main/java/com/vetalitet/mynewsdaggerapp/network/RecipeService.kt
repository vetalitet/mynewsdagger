package com.vetalitet.mynewsdaggerapp.network

import com.vetalitet.mynewsdaggerapp.network.model.RecipeDto
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface RecipeService {

    @GET("recipes/list")
    suspend fun getRecipes(@QueryMap params: Map<String, String>): RecipeDto

}
