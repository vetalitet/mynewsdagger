package com.vetalitet.mynewsdaggerapp.network.model

import com.google.gson.annotations.SerializedName

data class RecipeDto(
    @SerializedName("count") val count: Int? = null,
    @SerializedName("results") val results: List<Recipe> = listOf()
) {
    data class Recipe(
        @SerializedName("id") val id: Int? = null,
        @SerializedName("name") val name: String? = null,
        @SerializedName("thumbnail_url") val thumbnailUrl: String? = null,
        @SerializedName("created_at") val createdAt: Long? = null,
        @SerializedName("instructions") val instructions: List<Instruction>? = null,
        @SerializedName("tags") val tags: List<Tag>? = null,
        @SerializedName("sections") val sections: List<Section> = emptyList(),
        @SerializedName("user_ratings") val rating: Rating? = null,
        @SerializedName("nutrition") val nutrition: Nutrition? = null,
        @SerializedName("aspect_ratio") val aspectRatio: String? = null
    )

    data class Instruction(
        @SerializedName("id") val id: Int? = null,
        @SerializedName("display_text") val text: String? = null,
        @SerializedName("position") val position: Int? = null
    )

    data class Section(
        @SerializedName("components") val components: List<Component>? = null,
        @SerializedName("name") val name: String? = null,
        @SerializedName("position") val position: Int? = null
    )

    data class Rating(
        @SerializedName("score") val score: Float? = null,
        @SerializedName("count_positive") val positive: Int? = null,
        @SerializedName("count_negative") val negative: Int? = null
    )

    data class Nutrition(
        @SerializedName("calories") val calories: Int? = null,
        @SerializedName("carbohydrates") val carbohydrates: Int? = null,
        @SerializedName("fiber") val fiber: Int? = null,
        @SerializedName("protein") val protein: Int? = null,
        @SerializedName("fat") val fat: Int? = null,
        @SerializedName("sugar") val sugar: Int? = null
    )

    data class Component(
        @SerializedName("id") val id: Int? = null,
        @SerializedName("ingredient") val ingredient: Ingredient? = null,
        @SerializedName("measurements") val measurements: List<Measurements>? = null,
        @SerializedName("extra_comment") val extraComment: String? = null,
        @SerializedName("raw_text") val rawText: String? = null,
        @SerializedName("position") val position: Int? = null
    )

    data class Ingredient(
        @SerializedName("id") val id: Int? = null,
        @SerializedName("name") val name: String? = null,
    )

    data class Measurements(
        @SerializedName("id") val id: Int? = null,
        @SerializedName("quantity") val quantity: String? = null,
        @SerializedName("unit") val unit: Unit? = null
    )

    data class Unit(
        @SerializedName("system") val system: String? = null,
        @SerializedName("name") val name: String? = null
    )

    data class Tag(
        @SerializedName("type") val type: String? = null,
        @SerializedName("name") val name: String? = null,
        @SerializedName("id") val id: Int? = null,
        @SerializedName("display_name") val displayName: String? = null
    )

}
