package com.vetalitet.mynewsdaggerapp.network.states

import com.vetalitet.mynewsdaggerapp.network.HttpResult

sealed class ApiState<out T> {
    data class Error(val httpResult: HttpResult? = null, val code: Int? = null, val message: String? = null) : ApiState<Nothing>()
    data class Success<out T : Any>(val result: T? = null) : ApiState<T>()
}
