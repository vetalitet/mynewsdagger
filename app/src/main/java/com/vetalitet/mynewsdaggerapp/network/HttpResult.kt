package com.vetalitet.mynewsdaggerapp.network

enum class HttpResult {
    NO_CONNECTION,
    TIMEOUT,
    BAD_RESPONSE,
    SERVER_ERROR,
    MONTH_CONNECTIONS_LIMIT,
    NOT_DEFINED
}
