package com.vetalitet.mynewsdaggerapp.cache

enum class FetchingStrategy {
    NETWORK_ONLY, CACHE_FIRST, CACHE_ONLY
}