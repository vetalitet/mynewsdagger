package com.vetalitet.mynewsdaggerapp

import android.app.Application
import android.content.Context
import androidx.work.Configuration
import androidx.work.WorkManager
import androidx.work.WorkerFactory
import com.vetalitet.mynewsdaggerapp.di.CoreComponent
import com.vetalitet.mynewsdaggerapp.di.DaggerCoreComponent
import javax.inject.Inject

class BaseApplication : Application() {

    lateinit var coreComponent: CoreComponent

    override fun onCreate() {
        super.onCreate()
        coreComponent = DaggerCoreComponent
            .builder()
            .application(this)
            .build()
        WorkManager.initialize(this, Configuration.Builder().setWorkerFactory(coreComponent.factory()).build())
    }

    companion object {
        @JvmStatic
        fun coreComponent(context: Context)= (context.applicationContext as? BaseApplication)?.coreComponent
    }

}
